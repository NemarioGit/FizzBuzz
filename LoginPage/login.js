
const loginForm = document.getElementById("loginForm");
const loginButton = document.getElementById("loginFormSubmit");

//Wait for player's click on button "Login" and redirect player to the game's page
loginButton.addEventListener("click", (e) => {
    e.preventDefault();
    sessionStorage.setItem('username', loginForm.username.value);
    window.location.href = "../game/index.html";
})
