'use strict'

const scoreTXT = document.getElementById('scoreHolder');
const nicknameHolder = document.getElementById('nicknameHolder')
let currentScore = 0;
let scoreString = "  0  "

//This string is used for saving server's database request
let req = "http://basic-web.dev.avc.web.usf.edu/"; 

//Get player's nickname and score on the game's loading.
window.onload = () => {
    nicknameHolder.textContent = sessionStorage.getItem("username");  
    //Adding required username to the request string
    req = "http://basic-web.dev.avc.web.usf.edu/" + sessionStorage.getItem("username");
    get(req).then(function(response) {
        if(response.status == 200) {
            //User found
            currentScore = response.data.score; //Get user's score.          
            alert("You have successfully logged in.");
        }
        else {
            //User not found.
            post(req, { score: 0 }); //Create a new user.
            alert("You have successfully signed up.");
        }
        currentScore = currentScore % 101;
        scoreString = "  " + getScoreFormatting() + "  "  
        scoreTXT.textContent = scoreString; 
    });
};

//Add 1 point to player's score, display new value
function addScore() {
    currentScore = (currentScore + 1) % 101;
    scoreString = "  " + getScoreFormatting() + "  ";
    scoreTXT.textContent = scoreString;
    sendScore();
}

//Send player's score to the server
function sendScore() {
    post(req, { score: currentScore }).then(function(response){
        switch(response.status){
            case 200:
                //User was updated successfully.
                //response.data will be the same as returned by get(), and should contain the updated data.
                currentScore = response.data.score;
                break;
            case 201:
                //A new user was successfully created. Otherwise same as status 200.
                currentScore = response.data.score;
                break;
            case 400:
                //Bad request. Most likely your data that you sent (in this case dataToSend) was formatted incorrectly, or you supplied a negative score value.
                //response.data will be: { Error: "error message" }
                alert("Error: Bad request")
                break;
            case 500:
                //Something went wrong on the server, such as the database got deleted somehow. This should never happen.
                //response.data will be the same as status 400.
                alert("Error: Something went wrong on the server")
                break;
        }
    });
}

//This function checks if score is divisible by fifteen/five/three and returns appropriate result
function getScoreFormatting() {
    if (currentScore == 0) return 0;
    if (currentScore % 15 == 0) {
        return "Fizz Buzz";
    }
    else{
        if (currentScore % 5 == 0) {
            return "Buzz";
        }
        else{
            if (currentScore % 3 == 0) {
                return "Fizz";
            }
            else{
                return currentScore;
            }
        }
    }
}

//Get data from the server
function get(url) {
    return new Promise((resolve, reject) => {     
      const http = new XMLHttpRequest();
      http.onload = function() {
        resolve({ status: http.status, data: JSON.parse(http.response) });
      };
      http.open("GET", url);
      http.send();
    });
}

//Send data to the server
function post(url, data) {
    data = JSON.stringify(data);
    return new Promise((resolve, reject) => {
        let http = new XMLHttpRequest();
        http.onload = function() {
            resolve({ status: http.status, data: JSON.parse(http.response) });
        };
        http.open("POST", url);
        //Make sure that the server knows we're sending it json data.
        http.setRequestHeader("Content-Type", "application/json"); 
        http.send(data);      
    });
}
